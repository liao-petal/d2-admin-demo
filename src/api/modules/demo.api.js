export default ({ service, request, serviceForMock, requestForMock, mock, faker, tools }) => ({
  GET_LISTS (data = {}) {
    // 接口请求
    return request({
      url: '/api/getItemList',
      method: 'get',
      data
    })
  }
})
